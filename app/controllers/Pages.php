<?php require APPROOT . "/views/includes/header.php"; ?>

<h2>Home</h2>
<hr>
<h4>Our Social Sites</h4>
<div class="row justify-content-center">
    <div>
        <a href="https://twitter.com/PlaceDogs1" class="btn btn-default btn-lg">
            <i class="fa fa-twitter">
                <span class="network-name">Twitter</span>
            </i>
        </a>
    </div>
    <div>
        <a href="https://www.facebook.com/PlaceDogs-215116269101507/" class="btn btn-default btn-lg">
            <i class="fa fa-facebook">
                <span class="network-name">Facebook</span>
            </i>
        </a>
    </div>
    <div>
        <a href="https://placedogswebsite.tumblr.com/" class="btn btn-default btn-lg">
            <i class="fa fa-tumblr">
                <span class="network-name">Tumblr</span>
            </i>
        </a>
    </div>
</div>

<div class="para2">
    <h5>About Us</h5>
    This website was created for web developers and dog lovers to use for non copyrighted pictures that people wish to use. This is the dog version of a well known website "Placekitten", each picture has been willingly given from people all over the globe.
</div>

<div class="para2">
    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Placeat aperiam fugit deleniti ea aliquid tempora delectus enim
    minus magni exercitationem velit obcaecati corrupti ducimus facere recusandae eos adipisci accusantium nesciunt,
    sint nihil veniam voluptatum commodi sapiente! Iste autem praesentium, voluptates illo veniam expedita id consectetur
    culpa, harum nobis at ut mollitia eveniet non? Nesciunt, numquam? Perferendis pariatur iure inventore mollitia, necessitatibus
    quasi error animi incidunt eligendi dolor? Accusantium doloremque facere et inventore, quod tempore amet aliquid
    praesentium dolor, eius reprehenderit vitae sed unde sequi. Optio sit ex pariatur quia id vel esse excepturi? Doloribus
    atque nemo maiores debitis corporis nisi nihil doloremque, adipisci ipsa animi, minus vero quae eius eos et numquam
    soluta deleniti architecto quod explicabo nulla? Quibusdam reprehenderit, iste enim ipsam voluptatibus eligendi maiores
    quidem consequuntur vel, sequi saepe praesentium. Magni, ipsum eos ad maxime praesentium unde mollitia beatae accusamus
    magnam provident ipsam cum, nam sequi optio minus!
</div>

<div class="para2">
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil itaque quas consectetur quidem. Autem labore et tempore fugit
    nesciunt nemo omnis! Voluptate vel debitis earum quam repellat esse blanditiis amet nostrum, doloremque illo mollitia
    sapiente numquam, distinctio vero, porro voluptatibus dolore corporis soluta veniam hic perferendis similique facilis!
    Excepturi nostrum culpa necessitatibus iure, delectus magnam molestiae quo a porro nobis, deserunt voluptatum odit!
    Cupiditate vitae et possimus asperiores, illo sit voluptas! Aliquid fugit temporibus quia tempore impedit maiores
    vel at vero quasi dignissimos accusamus atque quas nostrum incidunt porro officiis repellendus voluptatibus, magni
    ad. Porro molestias corrupti velit laborum expedita nobis maxime? Reprehenderit sed aperiam accusantium. Odio, impedit
    id quibusdam corporis ratione commodi officiis molestias explicabo! Dolor alias hic distinctio velit quisquam est
    eligendi beatae debitis voluptatibus? Labore debitis ad, sint rerum illum cum corporis, atque neque doloremque earum
    voluptatum sunt! Ea beatae aut optio accusantium a velit. Eius, eveniet.
</div>

<div class="para2">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque soluta magni esse doloribus quae perferendis modi dolorem?
    Quidem dolore ab consectetur placeat velit consequuntur, reprehenderit cumque alias nisi reiciendis molestiae blanditiis
    voluptas nesciunt in perspiciatis expedita assumenda quis provident nostrum est! Quaerat autem adipisci nobis mollitia,
    tempore quos impedit ut nisi sunt porro fugit molestiae harum voluptatem, fugiat recusandae accusamus quo. A nobis
    pariatur modi velit libero accusamus delectus repellat tempora, iusto reiciendis quisquam commodi magnam, fugit quae
    aut aperiam laboriosam incidunt. Veniam, maiores alias numquam consequuntur illum laudantium dolorem deleniti obcaecati
    officiis repellendus dignissimos laborum dolore autem ipsum dicta, non tenetur. Voluptatibus consequatur qui rem,
    molestiae debitis quis doloremque ratione temporibus veritatis minus. Et placeat hic ratione vel, saepe repellat
    voluptas magnam deleniti asperiores quas! Rerum officiis est quaerat deserunt voluptatum placeat fuga maiores pariatur
    illum dicta ex ipsa, distinctio soluta exercitationem voluptatibus ab reiciendis, expedita consequatur nulla eligendi.
</div>

<div class="para3">Add your own dog to our ever growing collection! attach the image and give a small description to be featured on Dog Blog :)</div>

<div class="container">
        <form action="action_page.php">

            <label for="fname">First Name</label>
            <input type="text" id="fname" name="firstname" placeholder="First name">

            <label for="lname">Last Name</label>
            <input type="text" id="lname" name="lastname" placeholder="Last name">

            <label for="email">Email Address</label>
            <input type="text" id="email" name="email" placeholder="Email address">

            <label for="file">Choose file to upload</label>
            <input type="file" id="file" name="file" multiple>

            <label for="subject">Subject</label>
            <textarea id="subject" name="subject" placeholder="Write something about your dog here. . " style="height:200px"></textarea>

            <input type="submit" value="Submit">

        </form>
</div>

<?php require APPROOT . "/views/includes/footer.php"; ?>