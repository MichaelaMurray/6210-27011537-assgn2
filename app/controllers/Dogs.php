
<?php 

include(APPROOT . '/helper/helperfunctions.php');

class Pages extends Controller {
    public function __construct () {

        $this->dog = $this->model('Dogs_');

    }

    public function index() {

        $dog = $this->dog->getAllDogs();
        $title = $this->dog->title();

        $data = [
            'title' => $title,
            'dog' => $dog
        ];

        $this->view('pages/pictures', $data);
    }
}
?>