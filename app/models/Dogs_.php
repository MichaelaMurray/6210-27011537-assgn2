<?php 

    class Dogs_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function getAllDogs() {

            $this->db->query("SELECT * FROM tbl_dogs");
            return $this->db->resultSet();

        }

    }

?>