<?php

    class People {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function title() {
            return "Show All People";
        }

        public function getAllPeople() {
            $this->db->query('SELECT * FROM tbl_people');
            return $this->db->resultSet();
        }

        public function getSinglePerson($id) {
            $this->db->query('SELECT * FROM tbl_people WHERE ID = :id');
            $this->db->bind(":id", $id);
            return $this->db->resultSet();
        }

    }

?>