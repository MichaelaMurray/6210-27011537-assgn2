<?php 

    class Index_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function getAllIndex() {

            $this->db->query("SELECT * FROM tbl_index");
            return $this->db->resultSet();

        }

    }

?>