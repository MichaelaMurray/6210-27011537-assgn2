<?php require APPROOT . "/views/includes/header.php"; ?>

    <h2>Dog Blog</h2>

    <hr>

    <?php 

        foreach($data["dogs"] as $dogs) {

            $joined = $dogs["DOGADDED"]

            ?>
       

        <div class="imgalign">
            <img src="<?php echo URLROOT . "images/andy.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="Andy">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
        <div class="para">
            <?php echo $dogs['DOGDESCRIPT']?>
            <?php echo $dogs['DOGADDED']?>
        </div>

        <div class="imgalign">
            <img src="<?php echo URLROOT . "images/hudson.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="Hudson">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
        <div class="para">

            <?php echo $dogs['DOGDESCRIPT']?>
            <?php echo $dogs['DOGADDED']?>

        </div>

        <div class="imgalign">
            <img src="<?php echo URLROOT . "images/bella.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="Bella">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
        <div class="para">

            <?php echo $dogs['DOGDESCRIPT']?>
            <?php echo $dogs['DOGADDED']?>

        </div>

        <div class="imgalign">
            <img src="<?php echo URLROOT . "images/jack.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="Jack">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
        <div class="para">

            <?php echo $dogs['DOGDESCRIPT']?>
            <?php echo $dogs['DOGADDED']?>

        </div>

        <div class="imgalign">
            <img src="<?php echo URLROOT . "images/kai.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="Kai">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
        <div class="para">

            <?php echo $dogs['DOGDESCRIPT']?>
            <?php echo $dogs['DOGADDED']?>

        </div>

        <div class="imgalign">
            <img src="<?php echo URLROOT . "images/karma.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="Karma">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
        <div class="para">

            <?php echo $dogs['DOGDESCRIPT']?>
            <?php echo $dogs['DOGADDED']?>

        </div>

        <div class="imgalign">
            <img src="<?php echo URLROOT . "images/molly.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="Molly">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
        <div class="para">

            <?php echo $dogs['DOGDESCRIPT']?>
            <?php echo $dogs['DOGADDED']?>
            
        </div>

        <?php
    }

?>

<?php require APPROOT . "/views/includes/footer.php"; ?>