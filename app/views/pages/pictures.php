<?php require APPROOT . "/views/includes/header.php"; ?>

    <h2>Pictures</h2>
    <hr>

    <?php 

    foreach($data["dogs"] as $dogs) {

        ?>


    <a href="images/andy.jpg">
        <div class="img">
            <img src="<?php echo URLROOT . "images/andy.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="andy">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
    </a>

    <a href="images/hudson.JPG">
        <div class="img">
            <img src="<?php echo URLROOT . "images/hudson.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="hudson">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
    </a>

    <a href="images/bella.JPG">
        <div class="img">
            <img src="<?php echo URLROOT . "images/bella.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="bella">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
    </a>

    <a href="images/jack.JPG">
        <div class="img">
            <img src="<?php echo URLROOT . "images/jack.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="jack">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
    </a>

    <a href="images/kai.JPG">
        <div class="img">
            <img src="<?php echo URLROOT . "images/kai.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="kai">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
    </a>

    <a href="images/karma.JPG">
        <div class="img">
            <img src="<?php echo URLROOT . "images/karma.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="karma">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
    </a>

    <a href="images/molly.JPG">
        <div class="img">
            <img src="<?php echo URLROOT . "images/molly.jpg" . $dogs["DOGIMAGE"] . ".jpg"?>" alt="molly">
            <h3 class="name"><?php echo $dogs['FNAME']?></h3>
        </div>
    </a>


        <?php
    }

?>
<?php require APPROOT . "/views/includes/footer.php"; ?>