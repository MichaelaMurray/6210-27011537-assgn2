use container-db;

DROP TABLE IF EXISTS tbl_posts;

CREATE TABLE tbl_posts (

    ID INT(11) AUTO_INCREMENT,
    FNAME VARCHAR(255) NOT NULL,
    DOGIMAGE VARCHAR(255) NOT NULL,
    DOGDESCRIPT varchar (255),
    DOGADDED date,
    PRIMARY KEY (ID)

) AUTO_INCREMENT = 1;

INSERT INTO tbl_posts (TITLE, CONTENT) VALUES ('Andy', 'andy.jpg', 'Andy is a Chocolate Labrador. He loves Cheerios, chasing squirrels and barking for no reason at 4am.', '2018-03-02',)
INSERT INTO tbl_posts (TITLE, CONTENT) VALUES ('Hudson', 'hudson.jpg', 'Husdson is a long haired German Shepard. Hudson likes to annoy his brother and sister, and his favourite thing to do is sulk in the corner when he doesnt get his way.', '2018-02-05')
INSERT INTO tbl_posts (TITLE, CONTENT) VALUES ('Bella', 'bella.jpg', 'Bella is a Short Haired German Shepard. She loves being centre of attention and will bark at thin air to make sure she gets the attention she deserves.', '2018-04-23')
INSERT INTO tbl_posts (TITLE, CONTENT) VALUES ('Jack', 'jack.jpg', 'Jack is a Black Labrador and big brother to Molly and Hudson. He loves taking his favourite ball with him everywhere he goes to show it off to the ladies.', '2018-03-03')
INSERT INTO tbl_posts (TITLE, CONTENT) VALUES ('Kai', 'kai.jpg', 'Kai is a mixture of Shih Tzu, Maltese, and Bichon. He gets spoiled to pieces by his family, and spends most his days begging for food or causing havock.', '2018-05-28')
INSERT INTO tbl_posts (TITLE, CONTENT) VALUES ('Karma', 'karma.jpg', 'Karma is a Rottweiler. She loves going for long runs, destroying all her toys and making big holes in the backyard.', '2018-04-13')
INSERT INTO tbl_posts (TITLE, CONTENT) VALUES ('Molly', 'molly.jpg', 'Molly is a Short Haired German Shepard. She loves bossing around her brothers: Jack and Hudson, being the queen of her house and stealing toys.', '2018-05-19')