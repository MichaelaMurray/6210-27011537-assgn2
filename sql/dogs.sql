use container-db;

DROP TABLE IF EXISTS tbl_dogs;

CREATE TABLE IF NOT EXISTS tbl_dogs (

    ID INT(11) AUTO_INCREMENT,
    FNAME VARCHAR(255) NOT NULL,
    DOGIMAGE VARCHAR(255) NOT NULL,   
    DOGDESCRIPT VARCHAR(255) NOT NULL,
    PRIMARY KEY (ID)

) AUTO_INCREMENT = 1;

INSERT INTO tbl_people (FNAME, DOGIMAGE, DOGADDED, DOGDESCRIPT) VALUES ('Andy', 'andy.jpg', '2018-03-02', 'Andy is a Chocolate Labrador. He loves Cheerios, chasing squirrels and barking for no reason at 4am.');
INSERT INTO tbl_people (FNAME, DOGIMAGE, DOGADDED, DOGDESCRIPT) VALUES ('Hudson', 'hudson.jpg', '2018-02-05', 'Husdson is a long haired German Shepard. Hudson likes to annoy his brother and sister, and his favourite thing to do is sulk in the corner when he doesnt get his way.');
INSERT INTO tbl_people (FNAME, DOGIMAGE, DOGADDED, DOGDESCRIPT) VALUES ('Bella', 'bella.jpg', '2018-04-23', 'Bella is a Short Haired German Shepard. She loves being centre of attention and will bark at thin air to make sure she gets the attention she deserves.');
INSERT INTO tbl_people (FNAME, DOGIMAGE, DOGADDED, DOGDESCRIPT) VALUES ('Jack', 'jack.jpg', '2018-03-03', 'Jack is a Black Labrador and big brother to Molly and Hudson. He loves taking his favourite ball with him everywhere he goes to show it off to the ladies.');
INSERT INTO tbl_people (FNAME, DOGIMAGE, DOGADDED, DOGDESCRIPT) VALUES ('Kai', 'kai.jpg', '2018-05-28', 'Kai is a mixture of Shih Tzu, Maltese, and Bichon. He gets spoiled to pieces by his family, and spends most his days begging for food or causing havock.');
INSERT INTO tbl_people (FNAME, DOGIMAGE, DOGADDED, DOGDESCRIPT) VALUES ('Karma', 'karma.jpg', '2018-04-13', 'Karma is a Rottweiler. She loves going for long runs, destroying all her toys and making big holes in the backyard.');
INSERT INTO tbl_people (FNAME, DOGIMAGE, DOGADDED, DOGDESCRIPT) VALUES ('Molly', 'molly.jpg', '2018-05-19', 'Molly is a Short Haired German Shepard. She loves bossing around her brothers: Jack and Hudson, being the queen of her house and stealing toys.');

