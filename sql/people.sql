USE container-db;

DROP TABLE IF EXISTS tbl_people;

CREATE TABLE tbl_people (

    ID INT(11) NOT NULL AUTO_INCREMENT,
    FNAME VARCHAR(255) NOT NULL,
    EMAIL VARCHAR(255) NOT NULL,
    CMNT VARCHAR(100) NOT NULL,
    DOGIMAGE VARCHAR(255) NOT NULL, 
    PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

INSERT INTO tbl_people (FNAME,LNAME, EMAIL, IMG, CMNT) VALUES ('Ava', 'Langley', 'liliane52@example.org', 'hudson.jpg', 'Husdson is a long haired German Shepard. Hudson likes to annoy his brother and sister, and his favourite thing to do is sulk in the corner when he doesnt get his way.');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL, IMG, CMNT) VALUES ('Cleo', 'Meadows', 'ksanford@example.net', 'bella.jpg', 'Bella is a Short Haired German Shepard. She loves being centre of attention and will bark at thin air to make sure she gets the attention she deserves.');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL, IMG, CMNT) VALUES ('Jolie', 'Jacobson', 'watsica.bennie@example.org', 'jack.jpg', "Jack is a Black Labrador and big brother to Molly and Hudson. He loves taking his favourite ball with him everywhere he goes to show it off to the ladies.");
INSERT INTO tbl_people (FNAME,LNAME, EMAIL, IMG, CMNT) VALUES ('Ella', 'Ballard', 'bruce.swaniawski@example.net', 'kai.jpg', 'Kai is a mixture of Shih Tzu, Maltese, and Bichon. He gets spoiled to pieces by his family, and spends most his days begging for food or causing havock.');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL, IMG, CMNT) VALUES ('Cailin', 'Ellison', 'elnora41@example.org', 'karma.jpg', 'Karma is a Rottweiler. She loves going for long runs, destroying all her toys and making big holes in the backyard.');
INSERT INTO tbl_people (FNAME,LNAME, EMAIL, IMG, CMNT) VALUES ('Quin', 'Brooks', 'elda16@example.org', 'molly.jpg', 'Molly is a Short Haired German Shepard. She loves bossing around her brothers: Jack and Hudson, being the queen of her house and stealing toys.');
